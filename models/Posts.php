<?php

/**
 * @property mysqli $this->db
 */

namespace models;

use core\Model;

class Posts extends Model
{
    public static function tableName()
    {
        return 'posts';
    }

    public function findAll()
    {
        if(is_string(self::tableName())) {
            $query = <<<QUERY
SELECT t1.* , count(t2.id) as 'comments_count' 
FROM `posts` as t1 
left join comments as t2 on t1.id = t2.post_id 
GROUP by t1.id 
ORDER by t1.date_posted DESC;
QUERY;
            ;
//Данная запись требует родной библиотеки MySQLnd. Установка на Ubuntu: sudo apt-get install php5-mysqlnd , и перезапустить Apache
// http://stackoverflow.com/questions/11664536/fatal-error-call-to-undefined-method-mysqli-resultfetch-all
//          $result = $this->db->query($query)->fetch_all(MYSQLI_ASSOC);
//Универсальное решение:
            $result = [];
            $queryResult = $this->db->query($query);
            while ($row = $queryResult->fetch_assoc()) {
                $result []= $row;
            }
            return  $result;
        }
        return null;
    }

    public function findPopular($count)
    {
        if(is_string(self::tableName())) {
            $query = <<<QUERY
SELECT t1.*, count(t2.id) as 'comments_count'
FROM posts as t1 
left join comments as t2 on t1.id = t2.post_id
GROUP by t1.id
ORDER by comments_count DESC, t1.date_posted DESC
LIMIT $count;
QUERY;
;
//Данная запись требует родной библиотеки MySQLnd. Установка на Ubuntu: sudo apt-get install php5-mysqlnd , и перезапустить Apache
// http://stackoverflow.com/questions/11664536/fatal-error-call-to-undefined-method-mysqli-resultfetch-all
//          $result = $this->db->query($query)->fetch_all(MYSQLI_ASSOC);
//Универсальное решение:
            $result = [];
            $queryResult = $this->db->query($query);
            while ($row = $queryResult->fetch_assoc()) {
                $result []= $row;
            }
            return  $result;
        }
        return [];
    }

    public function findOne($id)
    {
        if(is_string(self::tableName())) {
            $query = <<<QUERY
SELECT t1.*, count(t2.id) as 'comments_count'
FROM posts as t1 
left join comments as t2 on t1.id = t2.post_id
where t1.id = $id
GROUP by t1.id
QUERY;
            return $this->db->query($query)->fetch_assoc();
        }
        return null;
    }

    public function newPost(array $post)
    {
        $date = time();
        //Рандомная картинка из доступных
        $imgArr = [
            '/uploads/slide1-dark.jpg',
            '/uploads/slide2-dark.jpg',
            '/uploads/slide3-dark.jpg',
        ];
        $image_src = $imgArr[array_rand($imgArr)];
        return $this->db->query("insert into posts(user_name, post_topic, post_body, image_src, date_posted) values ('${post['author']}', '${post['topic']}', '${post['topic']}', '${image_src}', ${date})");
    }
}