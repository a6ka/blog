<?php

namespace models;

use core\Model;

class Comments extends Model
{
    public static function tableName()
    {
        return 'comments';
    }

    public function findAll($post_id)
    {
        $query = 'select * from ' . self::tableName(). " where post_id = ${post_id} order by date_posted";
//Данная запись требует родной библиотеки MySQLnd. Установка на Ubuntu: sudo apt-get install php5-mysqlnd , и перезапустить Apache
// http://stackoverflow.com/questions/11664536/fatal-error-call-to-undefined-method-mysqli-resultfetch-all
//          $result = $this->db->query($query)->fetch_all(MYSQLI_ASSOC);
//Универсальное решение:
        $result = [];
        $queryResult = $this->db->query($query);
        while ($row = $queryResult->fetch_assoc()) {
            $result []= $row;
        }
        if($result) {
            $result = $this->commentsSort($result);
        }
        return $result;
    }

    private function commentsSort(array $result)
    {
        $links = array();
        $tree = array();
        $count = count( $result );
        for( $q = 0; $q < $count ; $q++ )
        {
            $elem = $result[$q];
            if( is_null($elem['parent_comment_id']) )
            {
                $tree[$elem['id']] = $elem;
                $links[$elem['id']] = &$tree[$elem['id']];
            }
            else
            {
                $links[$elem['parent_comment_id']]['childrens'][$elem['id']] = $elem;
                $links[$elem['id']] = &$links[$elem['parent_comment_id']]['childrens'][$elem['id']];
            }
        }

        return $tree;
    }

    public function newComment(array $post)
    {
        $date = time();
        $replyTo = !empty($post['reply_to'])?$post['reply_to']:'NULL';

        return $this->db->query("insert into comments(post_id, user_name, post_body, date_posted, parent_comment_id) values (${post['post_id']}, '${post['user_name']}', '${post['post_body']}', ${date}, ${replyTo})");
    }
}