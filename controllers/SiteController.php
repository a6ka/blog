<?php

namespace controllers;

use core\Connector;
use core\Controller;
use models\Comments;
use models\Posts;

class SiteController extends Controller
{
    public function actionIndex()
    {
        //Создаем подключение к БД (класс mysqli)
        $db = Connector::connect();

        $model = new Posts($db);
        //Добавление нового сообщения
        if(isset($_POST['new_post'])) {
            $model->newPost($_POST);
        }

        $this->template = 'default';
        return $this->render('index', [
            'topPosts' => $model->findPopular(5),
            'allPosts' => $model->findAll(),
        ]);
    }

    public function actionBlog($id)
    {
        //Создаем подключение к БД (класс mysqli)
        $db = Connector::connect();

        $model = new Posts($db);
        $comments = new Comments($db);

        //Добавление нового комментария
        if(isset($_POST['new_comment'])) {
            $comments->newComment($_POST);
        }

        $this->template = 'view';
        return $this->render('view', [
            'model' => $model->findOne($id),
            'comments' => $comments->findAll($id),
        ]);
    }
}