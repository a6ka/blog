<?php

namespace core;


class Connector
{
   private static $db;

   public static function connect()
   {
       $config = $GLOBALS['config']['db'];
       if($config['driver'] == 'mysql') {
           self::$db = mysqli_connect($config['host'], $config['login'], $config['password'], $config['dbname']);
           if (!self::$db) {
               die("Не удалось подключиться к MySQL");
           }
           if(self::$db) {
               //Кодировки
               self::$db->query("SET NAMES 'utf8'");
               self::$db->query("SET CHARACTER SET 'utf8'");
               self::$db->query("SET SESSION collation_connection = 'utf8_unicode_ci'");
           }

       } else {
           die("Данный драйвер не поддерживается.");
       }
       return self::$db;
   }
}