<?php

namespace core;

class View {

    public $view;
    public $viewPath;
    public $defaultExtension = 'php';

    function __construct($view, $viewPath)
    {
        $this->view = $view;
        $this->viewPath = $viewPath;
    }

    public function render($view, $params = []) {
        $viewFile = $this->findViewFile();
        return $this->renderFile($viewFile, $params);
    }

    public function findViewFile() {
        return $this->viewPath . DIRECTORY_SEPARATOR . $this->view . '.' . $this->defaultExtension;
    }

    public function renderFile($viewFile, $params = []) {
        if (is_file($viewFile)) {
            return $this->renderPhpFile($viewFile, $params);
        }
        return 'View file not found';
    }

    public function renderPhpFile($_file_, $_params_ = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        require($_file_);

        return ob_get_clean();
    }
}
