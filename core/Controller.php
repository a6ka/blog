<?php

namespace core;

class Controller
{
    public $id;
    public $model;
    public $_view;
    public $_viewPath;
    public $template = 'default';

    function __construct()	{
        //Такая строка работала, но вызывала предупреждение Strict standards: Only variables should be passed by reference
//        $this->id = strtolower(str_replace( 'Controller', '', array_pop( explode('\\', get_class($this)) ) ) );
        $className = get_class($this);
        $ClassNameArr = explode('\\', $className);
        $className = '';
        if(count($ClassNameArr)) {
            $className = array_pop($ClassNameArr);
        }
        $className = str_replace( 'Controller', '', $className);
        $this->id = strtolower($className);

    }

    function render($view, $params = []) {
        $this->_view = new View($view, $this->getViewPath());
        //use in layout
        $content = $this->_view->render($view, $params);
        include_once 'views/layouts/' . $this->template . '.php';
    }

    public function getViewPath()
    {
        if ($this->_viewPath === null) {
            $this->_viewPath = 'views' . DIRECTORY_SEPARATOR . $this->id;
        }
        return $this->_viewPath;
    }
}