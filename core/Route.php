<?php

namespace core;

use controllers\SiteController;

class Route
{
    public static $controller_name = 'Site';
    public static $action_name = 'index';

    public static function start()
	{

		$routes = explode('/', $_SERVER['REQUEST_URI']);

		// получаем имя контроллера
		if ( !empty($routes[1]) )
		{	
			self::$controller_name = ucfirst($routes[1]);
		}
		
		// получаем имя экшена
		if ( !empty($routes[2]) )
		{
            self::$action_name = $routes[2];
		}

        // получаем ID блога
        $id = !empty($routes[3]) ? intval($routes[3]) : null;

		// добавляем префиксы
        self::$controller_name .= 'Controller';
        self::$action_name = 'action'.ucfirst(strtolower(self::$action_name)) ;



		// вставляем файл с классом контроллера
		$controller_path = 'controllers/'. self::$controller_name .'.php';
		if(file_exists($controller_path))
		{
			include $controller_path;
		}
		else
		{
			//Если контроллер не найден
			self::ErrorPage404();
		}
		
		// создаем контроллер
        $a = "\\controllers\\" . self::$controller_name;
		$controller = new $a;
		
		if(method_exists($controller, self::$action_name))
		{
			// вызываем действие контроллера
            $action_name = self::$action_name;
			$controller->$action_name($id);
		}
		else
		{
			// нет такого экшена
			self::ErrorPage404();
		}
	
	}
	
	public static function ErrorPage404()
	{
        header('HTTP/1.1 404 Not Found');
		header('Status: 404 Not Found');
		die();
    }
}