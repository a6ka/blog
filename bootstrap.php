<?php

use core\Route;

//Подключаем конфиг
include_once 'config/main.php';
//Автоподключение файлов
function __autoload($class_name) {
    if (file_exists(str_replace('\\', '/', $class_name) . '.php')) {
        require_once str_replace('\\', '/', $class_name) . '.php';
    }
}
//Старт ЧПУ
Route::start();