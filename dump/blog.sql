-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 20 2017 г., 02:33
-- Версия сервера: 5.7.11
-- Версия PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--
CREATE DATABASE IF NOT EXISTS `blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `blog`;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_body` text COLLATE utf8_unicode_ci NOT NULL,
  `date_posted` int(10) UNSIGNED NOT NULL,
  `parent_comment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_name`, `post_body`, `date_posted`, `parent_comment_id`) VALUES
(1, 1, 'Commentator 1', 'comment 1', 1489943993, NULL),
(2, 2, 'Commentator 2', 'comment 2', 1489943993, NULL),
(3, 3, 'Commentator 3', 'comment 3', 1489943993, NULL),
(4, 4, 'Commentator 4', 'comment 4', 1489943993, NULL),
(5, 5, 'Commentator 5', 'comment 5', 1489943993, NULL),
(6, 6, 'Commentator 6', 'comment 6', 1489943993, NULL),
(7, 7, 'Commentator 7', 'comment 7', 1489943993, NULL),
(8, 1, 'Commentator 8', 'comment 8', 1489943993, 1),
(9, 1, 'Commentator 1', 'comment 1', 1489943993, 8),
(10, 2, 'Commentator 2', 'comment 3', 1489943993, NULL),
(11, 1, 'Commentator 11', 'comment 11', 1489943994, NULL),
(12, 1, 'Alex', 'test comment', 1489969576, NULL),
(13, 1, 'Reply', 'Test', 1489974699, 12),
(14, 1, 'test', 'test', 1489974715, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_topic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_body` text COLLATE utf8_unicode_ci NOT NULL,
  `image_src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_posted` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `user_name`, `post_topic`, `post_body`, `image_src`, `date_posted`) VALUES
(1, 'User', 'Topic 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide1-dark.jpg', 1489941679),
(2, 'Admin', 'Topic 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide2-dark.jpg', 1489941682),
(3, 'User', 'Topic 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide1-dark.jpg', 1489941781),
(4, 'Admin', 'Topic 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide2-dark.jpg', 1489941782),
(5, 'User', 'Topic 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide1-dark.jpg', 1489941879),
(6, 'Admin', 'Topic 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide2-dark.jpg', 1489941882),
(7, 'User', 'Topic 7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide1-dark.jpg', 1489941981),
(8, 'Admin', 'Topic 8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in placerat magna, in porta elit. Etiam sit amet magna lorem. Nunc ut quam vel ligula fermentum bibendum. Cras condimentum ipsum tristique libero suscipit, ut pulvinar nibh pellentesque. Vestibulum tincidunt consequat nibh, at ullamcorper ligula tincidunt ut. Aliquam erat volutpat. Phasellus semper magna nec sem finibus, at sodales lacus efficitur.\r\n\r\nNullam sit amet metus sit amet ex accumsan tempus ac ac ligula. Fusce leo orci, dignissim sed eleifend in, viverra vitae enim. In ante ante, euismod non tortor id, accumsan mattis diam. Donec nec ornare ipsum, eu hendrerit turpis. Duis nec fringilla risus, id pellentesque mi. Mauris ullamcorper elit sed consectetur efficitur. Suspendisse auctor gravida elit, nec condimentum lorem efficitur vitae. Aenean rhoncus velit nec ante facilisis placerat. Mauris et nibh ut ipsum hendrerit molestie. Duis quis augue eget elit mollis sodales in at lectus. Mauris volutpat tortor a sapien rhoncus vehicula. Duis nec mauris eu metus finibus cursus at non dolor. Ut quis condimentum nisl, eget volutpat velit. Proin orci eros, rutrum sit amet tempus eu, sodales hendrerit purus. Donec porttitor odio eget lorem sodales, at consequat ligula accumsan.\r\n\r\nSed sit amet luctus nisi, et hendrerit metus. Sed sollicitudin vitae ante ut pretium. Aliquam sit amet magna ut erat dictum aliquet quis vitae augue. Ut at nisl nec risus mattis elementum et eu justo. Aenean hendrerit arcu felis, a tristique quam auctor nec. Maecenas tincidunt eleifend ipsum nec porta. Duis lorem magna, condimentum eu ligula ut, dapibus pulvinar massa. Cras facilisis erat sit amet tortor efficitur, vitae fermentum neque placerat. Sed et nunc convallis, congue eros quis, congue metus. In ornare quis urna sit amet condimentum. Nam congue nibh at velit porttitor, sed fringilla sapien porta.', '/uploads/slide2-dark.jpg', 1489941982);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `parent_comment_id` (`parent_comment_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`parent_comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
