<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog</title>

    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <!-- Library - Google Font Familys -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="/revolution/fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/revolution/fonts/font-awesome/css/font-awesome.min.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css">

    <!-- Library Css -->
    <link href="/css/lib.css" rel="stylesheet">

    <!-- Custom - Common CSS -->
    <link href="/css/plugins.css" rel="stylesheet">
    <link href="/css/elements.css" rel="stylesheet">
    <link href="/css/rtl.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <!--[if lt IE 9]>
    <script src="/js/respond.min.js"></script>
    <![endif]-->

</head>

<body data-offset="200" data-spy="scroll" data-target=".ownavigation">
<div class="main-container">
    <!-- Loader -->
    <div id="site-loader" class="load-complete">
        <div class="loader">
            <div class="loader-inner ball-clip-rotate">
                <div></div>
            </div>
        </div>
    </div><!-- Loader /- -->

    <header class="header_s header_s2">
        <!-- SidePanel -->
        <div id="slidepanel">
            <!-- Top Header -->
            <div class="container-fluid no-left-padding no-right-padding top-header">
                <!-- Container -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 welcome-line">
                            <h6>Welcome to my Blog!</h6>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 block-cnt">
                            <ul class="top-social">
                                <li><a href="#" title="Twitter"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <a href="#" title="Free Seo Analysis">special for v-jet</a>
                        </div>
                    </div>
                </div><!-- Container /- -->
            </div><!-- Top Header /- -->
            <!-- Middle Header -->
            <div class="container-fluid no-left-padding no-right-padding middle-header">
                <!-- Container -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <span><i class="icon icon-Phone2"></i><a href="tel:+380984330464">(+38) 098 433 0464</a></span>
                            <span><i class="icon icon-Mail"></i><a href="mailto:a6ka666@gmail.com">a6ka666@gmail.com</a></span>
                        </div>
                    </div>
                </div><!-- Container /- -->
            </div><!-- Middle Header /- -->
        </div><!-- SidePanel /- -->

        <!-- Ownavigation -->
        <nav class="navbar ownavigation">
            <!-- Container -->
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="/images/logo.png" alt="logo"></a>
                </div>
                <div class="menu-switch">
                    <a href="javascript:void(0);" title="menu"><i class="fa fa-bars"></i></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/" title="Home">Home</a></li>
                        <li><a href="#" title="About Us">About Us</a></li>
                        <li><a href="#" title="About Us">Contacts</a></li>
                    </ul>
                </div>
                <div id="loginpanel" class="desktop-hide">
                    <div class="right" id="toggle">
                        <a id="slideit" href="#slidepanel"><i class="fo-icons fa fa-inbox"></i></a>
                        <a id="closeit" href="#slidepanel"><i class="fo-icons fa fa-close"></i></a>
                    </div>
                </div>
            </div><!-- Container /- -->
        </nav><!-- Ownavigation /- -->
    </header>



    <main class="site-main">
        <?= $content?>
    </main>

    <!-- Footer Main -->
    <footer id="footer-main" class="footer-main footer-main-1 container-fluid no-left-padding no-right-padding">
        <!-- Container -->
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Widget About -->
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <aside class="widget widget_about">
                        <h3 class="widget-title">About Us</h3>
                        <p>Just two good ol' boys Wouldn't change if they could. Fightin' the system like a true modern day Robin Hood. Space. The final frontier. These are the voyages of the Starship Enterprise. Just two good ol' boys.</p>
                    </aside>
                    <div class="social-copy-widget">
                        <ul class="footer-social">
                            <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" title="Twiiter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                        <div class="copyright">
                            <p>Copy Rights <i class="fa fa-copyright"></i> 2016 Max All Rights Reserved</p>
                        </div>
                    </div>
                </div><!-- Widget About /- -->
                <!-- Widget Links -->
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <aside class="widget widget_links">
                        <h3 class="widget-title">Quick Links</h3>
                        <ul>
                            <li><a href="#" title="Case Studies">Case Studies</a></li>
                            <li><a href="#" title="Services">Services</a></li>
                            <li><a href="#" title="About Us">About Us</a></li>
                            <li><a href="#" title="Pricing Table">Pricing Table</a></li>
                            <li><a href="#" title="Blog">Blog</a></li>
                            <li><a href="#" title="Marketing">Marketing</a></li>
                            <li><a href="#" title="SEO Analysis">SEO Analysis</a></li>
                            <li><a href="#" title="Mobile Application">Mobile Application</a></li>
                        </ul>
                    </aside>
                </div><!-- Widget Links /- -->
                <!-- Widget Newsletter -->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-6">
                            <aside class="widget widget_everywhere">
                                <h3 class="widget-title">We are Everywhere</h3>
                                <div class="cnt-detail">
                                    <p><i class="icon icon-Phone2"></i><a href="tel:+380984330464">(+38) 098 433 0464</a></p>
                                    <p><i class="icon icon-Mail"></i><a href="mailto:a6ka666@gmail.com">a6ka666@gmail.com</a></p>
                                </div>
                            </aside>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-6">
                            <aside class="widget widget_newsletter">
                                <h3 class="widget-title">Our NewsLetter</h3>
                                <form>
                                    <input type="text" required="" placeholder="Enter Your Email..." class="form-control">
                                    <input type="submit" title="SUBSCRIBE" value="SUBSCRIBE">
                                </form>
                            </aside>
                        </div>
                    </div>
                </div><!-- Widget Newsletter /- -->
            </div><!-- Row /- -->
        </div><!-- Container /- -->
    </footer><!-- Footer Main /- -->

</div>

<!-- JQuery -->
<script src="/js/jquery-3.2.0.min.js"></script>

<!-- Library - Js -->
<script src="/js/lib.js"></script>

<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script type="text/javascript" src="/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

<!-- Library - Theme JS -->
<script src="/js/functions.js"></script>

</body>
</html>