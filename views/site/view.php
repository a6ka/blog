<?php
$commentLevel = 0;
function ShowTree( &$tree ){
    global $commentLevel;
    $commentLevel++;
    //Рандомная картинка из доступных
    $imgArr = [
        '/uploads/comment1.jpg',
        '/uploads/comment2.jpg',
        '/uploads/comment3.jpg',
        '/uploads/comment4.jpg',
        '/uploads/comment5.jpg',
        '/uploads/comment6.jpg',
        '/uploads/comment7.jpg',
        '/uploads/comment8.jpg',
    ];
    echo '<ol class="'. (($commentLevel == 1)?'comment-list':'children') .'">';
    foreach( $tree as &$item ){
        $image_src = $imgArr[array_rand($imgArr)];
        $textData = date('F', $item['date_posted']) . ' ' . date('j', $item['date_posted']) . ', ' . date('Y', $item['date_posted']);
        echo '<li id="comment-'. $item['id'] .'">';
        echo <<<COMMENT
<article class="comment-body">
<footer class="comment-meta">
    <div class="comment-author vcard">
        <img alt="Avtar" src="${image_src}" class="avatar avatar-72 photo"/>
    </div>
</footer>
<div class="comment-content">
    <h3 class="fn">${item['user_name']}</h3>
    <div class="comment-metadata">
        <a href="#">${textData}</a>
    </div>
    <p>${item['post_body']}</p>
</div>
<div class="reply">
    <a rel="nofollow" class="comment-reply-link" href="#comment-form" data-comment-id="${item['id']}">Reply</a>
</div>
</article>
COMMENT;

        if( ! empty($item['childrens']) )
            ShowTree( $item['childrens'] );
        echo '</li>';
    }
    echo '</ol>';
}
?>
<!-- Container -->
<div class="container">
    <!-- Row -->
    <div class="row">
        <!-- Content Area -->
        <div class="col-md-9 col-sm-7 col-xs-12 content-area content-left content-space">
            <?php if($model) :?>
                <article class="type-post">
                    <div class="entry-cover">
                        <img src="<?= $model['image_src']?>" alt="Blog" />
                    </div>
                    <div class="entry-header">
                        <div class="post-meta">
                            <div class="post-date"><a><?= date('j', $model['date_posted'])?><i>/</i><span><?= date('Y', $model['date_posted'])?> <span><?= strtoupper(date('M', $model['date_posted']))?></span></span></a></div>
                            <span class="byline">Author : <?=$model['user_name'];?></span>
                            <span><a href="#comments"><?=$model['comments_count'];?> Comments</a></span>
                        </div>
                        <h3 class="entry-title"><?= $model['post_topic']?></h3>
                        <p>&nbsp</p>
                        <p>&nbsp</p>
                    </div>
                    <div class="entry-content">
                        <p><?= nl2br($model['post_body']);?></p>
                    </div>
                </article>

                <!-- Comments Block -->
                <div class="col-md-12 col-sm-12 col-xs-12 no-left-padding no-right-padding comments-block">
                    <div id="comments" class="comments-area">
                        <h2 class="comments-title">Comments</h2>
                        <?php
                            if(count($comments)) {
                                ShowTree( $comments );
                            } else {
                                echo '<p>No comments yet. Be the first!</p><div class="padding-50"></div>';
                            }
                        ?>
                        <!-- .comment-list -->
                        <div class="comment-respond" id="comment-form">
                            <h2 class="comment-reply-title">Leave Your Comments Here</h2>
                            <form method="post" id="commentform" class="comment-form">
                                <p class="go-to-comment hide">Reply to: <a id="reply_link" href="#"></a> <span id="cancel_reply"><i class="fa fa-times" aria-hidden="true"></i></span></p>
                                <input type="hidden" name="reply_to" />
                                <input type="hidden" name="post_id" value="<?= $model['id']?>" />
                                <p class="comment-form-comment">
                                    <input id="user_name" name="user_name" placeholder="Name*" maxlength="255" required="required" type="text"/>
                                </p>
                                <p class="comment-form-comment">
                                    <textarea id="post_body" name="post_body" rows="4" placeholder="Your Message" required="required"></textarea>
                                </p>
                                <p class="form-submit">
                                    <input name="new_comment" class="submit" value="Post Comment" type="submit"/>
                                </p>
                            </form>
                        </div>
                    </div>
                </div><!-- Comments Block /- -->
            <?php else:?>
                <p>Статьи с таким ID не существует.</p>
            <?php endif;?>
        </div><!-- Content Area /- -->
        <!-- Widget Area -->
        <div class="col-md-3 col-sm-5 col-xs-12 widget-area sidebar-right">
            <!-- Subscribe With Us -->
            <aside class="widget widget_subscribe">
                <h3 class="widget-title">Subscribe For News</h3>
                <div class="subscribe-box">
                    <p>The weather started getting rough - the tiny ship was tossed. If not for the courage of the fearless.</p>
                    <div class="input-group">
                        <input class="form-control" placeholder="Your Name..." type="text" disabled>
                        <input class="form-control" placeholder="Email" type="text" disabled>
                        <input type="submit" class="btn btn-default" value="SUBSCRIBE" disabled/>
                    </div><!-- /input-group -->
                </div>
            </aside><!-- Subscribe With Us /- -->
            <!-- Widget: Tag Cloud -->
            <aside id="tag_cloud" class="widget widget_tag_cloud">
                <h3 class="widget-title">Popoular Tags</h3>
                <div class="tagcloud">
                    <a href="#" title="SEO Analysis">Тут</a>
                    <a href="#" title="Marketing">Могли</a>
                    <a href="#" title="Services">Быть</a>
                    <a href="#" title="Pricing Plan">Теги</a>
                    <a href="#" title="Case Studies">Но их нет :)</a>
                </div>
            </aside>
        </div><!-- Widget Area /- -->
    </div><!-- Row /- -->
</div><!-- Container -->