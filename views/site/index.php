<!-- Slider Section 2 -->
<div id="home-revslider" class="slider-section slider-section2 container-fluid no-padding">
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="rev_slider_wrapper">
        <div id="home-slider2" class="rev_slider" data-version="5.0">
            <ul>
                <?php foreach ($topPosts as $id => $post) :?>
                    <?php $id++;?>
                    <li>
                        <img src="<?= $post['image_src']?>" alt="slider" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-<?=$id;?>-layer-1"
                             data-x="['left','left','left','left']" data-hoffset="['380','140','70','30']"
                             data-y="['top','top','top','top']" data-voffset="['155','100','60','40']"
                             data-fontsize="['44','40','32','23']"
                             data-lineheight="['72','62','52','42']"
                             data-width="['900','900','900','400']"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[-105%];s:1000;e:Power4.slideInRight;"
                             data-transform_out="y:[100%];s:1000;s:1000;e:Power2.slideInRight;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 5; white-space: nowrap; letter-spacing: 1.1px; color:#fff; font-weight: bold;  font-family: 'Raleway', sans-serif;">
                            <?= $post['post_topic'];?>
                        </div>
                        <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-<?=$id;?>-layer-2"
                             data-x="['left','left','left','left']" data-hoffset="['380','140','70','30']"
                             data-y="['top','top','top','top']" data-voffset="['320','240','180','135']"
                             data-width="none"
                             data-height="none"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[0];s:1000;e:Power4.zoomIn;"
                             data-transform_out="y:[0];s:1000;s:1000;e:Power2.zoomIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 5; white-space: nowrap;"><img src="/images/slider-seprator.png" alt="Sep" />
                        </div>
                        <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-<?=$id;?>-layer-4"
                             data-x="['left','left','left','left']" data-hoffset="['380','140','70','30']"
                             data-y="['top','top','top','top']" data-voffset="['360','270','210','160']"
                             data-fontsize="['14','14','14','14']"
                             data-lineheight="['24','24','24','24']"
                             data-width="['750','750','750','400']"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="y:-50px;opacity:0;s:1000;e:Power4.easeOut;"
                             data-transform_out="opacity:0;s:1000;e:Power4.easeIn;s:1000;e:Power4.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="chars"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-elementdelay="0.05"
                             style="z-index: 5; white-space: nowrap; letter-spacing: 0.49px; color:#fff; font-family: 'Open Sans', sans-serif; font-weight: normal;">
                            <?= substr(nl2br($post['post_body']), 0, 100) . '...';?>
                        </div>
                        <div class="tp-caption NotGeneric-Button rev-btn  rs-parallaxlevel-0" id="slide-<?=$id;?>-layer-5"
                             data-x="['left','left','left','left']" data-hoffset="['380','140','70','30']"
                             data-y="['top','top','top','top']" data-voffset="['450','350','295','240']"
                             data-fontsize="['15','15','15','15']"
                             data-lineheight="['30','30','30','30']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[105%];s:1000;e:Power4.slideInLeft;"
                             data-transform_out="y:[100%];s:1000;s:1000;e:Power2.slideInLeft;"
                             data-style_hover="c:#fff;bg:#ffb300;"
                             data-start="3000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             data-responsive="off"
                             style="z-index: 10;padding:9px 35px 8px;border-radius: 0;border: none;border-left: 3px solid #ffb300; letter-spacing:0.75px; color: #fff; background-color: #0056b7; font-weight: 600; font-family: 'Open Sans', sans-serif; text-transform:uppercase; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="/site/blog/<?=$post['id'];?>">Read More</a>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div><!-- Slider Section 2 -->
<div class="clearfix"></div>
<div class="padding-60"></div>
<!-- Container -->
<div class="container">
    <!-- Row -->
    <div class="row">
        <!-- Content Area -->
        <div class="col-md-9 col-sm-7 content-area content-left content-space">
            <h3 class="title">BLOG</h3>
            <?php foreach ($allPosts as $post):?>
                <div class="type-post">
                    <div class="entry-cover">
                        <a href="/site/blog/<?=$post['id'];?>"><img src="<?=$post['image_src'];?>" alt="Blog" /></a>
                    </div>
                    <div class="entry-header">
                        <div class="post-meta">
                            <div class="post-date"><a><?= date('j', $post['date_posted'])?><i>/</i><span><?= date('Y', $post['date_posted'])?> <span><?= strtoupper(date('M', $post['date_posted']))?></span></span></a></div>
                            <span class="byline">Author : <?=$post['user_name'];?></span>
                            <span><?=$post['comments_count'];?> Comments</span>
                        </div>
                        <h3 class="entry-title"><a href="/site/blog/<?=$post['id'];?>"><?=$post['post_topic'];?></a></h3>
                        <p><?= substr(nl2br($post['post_body']), 0, 200) . '...';?></p>
                        <a href="/site/blog/<?=$post['id'];?>" title="Read More">Read More</a>
                    </div>
                </div>
            <?php endforeach;?>
            <nav class="ow-pagination text-center">
                <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                </ul>
            </nav>
        </div><!-- Content Area /- -->
        <!-- Widget Area -->
        <div class="col-md-3 col-sm-5 widget-area sidebar-right">
            <!-- Add new post form -->
            <aside class="widget widget_subscribe">
                <h3 class="widget-title">Add new post</h3>
                <div class="subscribe-box">
                    <form method="post">
                        <div class="input-group">
                            <input  name="author" class="form-control" placeholder="Your Name..." type="text" maxlength="255" required>
                            <input  name="topic" class="form-control" placeholder="Post Topic..." type="text" maxlength="255" required>
                            <textarea name="post" class="form-control" rows="10" placeholder="Post text" required></textarea>
                            <input type="submit" class="btn btn-default" value="ADD POST" name="new_post"/>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </aside><!-- Subscribe With Us /- -->
            <!-- Widget: Tag Cloud -->
            <aside id="tag_cloud" class="widget widget_tag_cloud">
                <h3 class="widget-title">Popoular Tags</h3>
                <div class="tagcloud">
                    <a href="#" title="SEO Analysis">Тут</a>
                    <a href="#" title="Marketing">Могли</a>
                    <a href="#" title="Services">Быть</a>
                    <a href="#" title="Pricing Plan">Теги</a>
                    <a href="#" title="Case Studies">Но их нет :)</a>

                </div>
            </aside>
        </div><!-- Widget Area /- -->
    </div><!-- Row /- -->
</div><!-- Container -->
